package JavaBasic;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;
import java.util.Collections;

public class Forexample3 {
    public static void main(String[] args) throws Exception {
        JavaBasic.Forexample3.arrayToArrayList();
        JavaBasic.Forexample3.arrayToArrayList1();
        JavaBasic.Forexample3.arrayToArrayList2();
    }
    public static void arrayToArrayList(){
        Integer[] integers = new Integer[]{5,6,7,8,9};
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(integers));
        System.out.println(arrayList);
    }
    public static void arrayToArrayList1(){
        Integer[] arr = {1,2,3,4,5,6,7};
        ArrayList<Integer> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arr);
        System.out.println(arrayList);
    }
    public static void arrayToArrayList2(){
        Integer[] arr = {6,7,8,9,10,11,13};
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (Integer i : arr){
            arrayList.add(i);
        }
        System.out.println(arrayList);
    }
}
