package JavaBasic;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;
import java.util.Collections;
public class ForExample4 {
    public static void main(String[] args) throws Exception {
        JavaBasic.ForExample4.toArray();
        JavaBasic.ForExample4.toArray1();
        JavaBasic.ForExample4.toArray2();
    }
    public static void toArray(){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        Integer[] results = arrayList.stream().toArray(size -> new Integer[size]);
        for (Integer i : results){
            System.out.println(i + " ");
        }
    }
    public static void toArray1(){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        Integer[] results = arrayList.stream().toArray(Integer[]::new);
        for (Integer i : results){
            System.out.println(i + " ");
        }
    }
    public static void toArray2(){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(7);
        arrayList.add(8);
        arrayList.add(9);
        int[] results = arrayList.stream().mapToInt(i -> i ).toArray();
        for (Integer i : results){
            System.out.println(i + " ");
        }
    }
}
