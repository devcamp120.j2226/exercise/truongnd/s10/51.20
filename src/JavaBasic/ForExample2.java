package JavaBasic;
import java.util.ArrayList;
import java.util.Random;

public class ForExample2 {
    public static void main(String[] args) throws Exception {
        Random random = new Random();
        Integer[] arrayList = new Integer[20];
        // create list number
        for (int i = 0; i<20; i++){
            arrayList[i] = Integer.valueOf(random.nextInt());
        }
        for (int i = 0; i< arrayList.length; i++){
            System.out.println(arrayList[i]);
        }
    }
}
