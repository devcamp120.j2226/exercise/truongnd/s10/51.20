package JavaBasic;
import java.util.ArrayList;

public class ForExample {
    public static void main(String[] args) throws Exception {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);
        arrayList.add(8);
        arrayList.add(9);
        arrayList.add(10);
        arrayList.add(11);
        arrayList.add(12);
        for (int i= 0; i < arrayList.size(); i++){
            System.out.println(arrayList.get(i));
        }
    }
}
